var path = 'icons/icon.png';
var aseKey = "kMo_p*0Y"; //必须是1/8/16位，可以自己指定。

// 返回灰色画布
function grey_img(src,callback){
　　/*创建一个canvas*/
	var canvas=document.createElement('canvas');
	var ctx=canvas.getContext('2d');
	var img=new Image();
	img.src=src;
	img.onload = function(){
		// 因为图片加载时异步的，所以需要采用回调的方式，不然图片宽高读取会变成0导致 Failed to execute 'getImageData' on 'CanvasRenderingContext2D' 报错
		canvas.height=img.height;
		canvas.width=img.width;
		ctx.drawImage(img,0,0);
		var imgdata=ctx.getImageData(0,0,canvas.width,canvas.height);var data=imgdata.data;
		/*灰度处理：求r，g，b的均值，并赋回给r，g，b*/
		for(var i=0,n=data.length;i<n;i+=4){
			var average=(data[i]+data[i+1]+data[i+2])/3;
			data[i]=average;
			data[i+1]=average;
			data[i+2]=average;
		}
		ctx.putImageData(imgdata,0,0);
		if(typeof callback === 'function'){
			/*将处理后的回调给调用者*/
			callback({canvas,ctx});
		}
	}
}

function getRootUrl(url) {
	return url.toString().replace(/^(.*\/\/[^\/?#]*).*$/, "$1");
}

// 加密
function encrypts(message){
    var encrypt = CryptoJS.AES.encrypt(message, CryptoJS.enc.Utf8.parse(aseKey), {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    }).toString();
	encrypt = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(encrypt));
    return encrypt;
}
//解密
function decrypts(encrypt){
	var decrypt = CryptoJS.enc.Base64.parse(encrypt).toString(CryptoJS.enc.Utf8);
    decrypt = CryptoJS.AES.decrypt(decrypt, CryptoJS.enc.Utf8.parse(aseKey), {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8);
	return decrypt;
    // //转成数字类型
    // var numDecrypt = parseInt(decrypt);
    // return numDecrypt;
}